#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'workspace.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
    print('-'*30)
    from user.models import *
    user1=Accounta.objects.filter(user_name__in = ['name1','name2'])
    print(user1)
    # try:
    #     Accounta.objects.get(account=10000)
    # except:
    #     print('没有满足的条件,抛出异常了')
    # try:
    #     Accounta.objects.get(isdelete=0)
    # except:
    #     print('多个对象满足条件,抛出异常')
