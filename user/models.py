from django.db import models
from datetime import datetime,date,time
# Create your models here.
class Accounta(models.Model):
    account = models.CharField(unique=True,primary_key=True,max_length=16)
    user_name = models.CharField(max_length=16,null=True,db_index=True)
    password =models.CharField(max_length=12)
    gender = models.PositiveSmallIntegerField(default=0)
    birthday = models.DateField(null=True)
    money = models.DecimalField(max_digits=10,decimal_places=2,default=0)
    email = models.EmailField(null=True)
    image = models.ImageField(null=True)
    createDatetime = models.DateTimeField(auto_now_add =datetime.now())
    updateDatetime = models.DateTimeField(auto_now_add =datetime.now())
    isdelete =models.BooleanField(default=False)
def build_user(num=100):
    for i in range(num):
        Accounta.objects.create(
            account='%05d' %i,
            user_name = 'name%s' %i,
            password = str(i),
            gender =i%2,
            birthday = datetime.now(),
            money = i**2+12.34*i+100,
            email = '%05d@qq.com' %i,
            createDatetime =datetime.now(),
            updateDatetime = datetime.now(),
            isdelete = i%2,
        )