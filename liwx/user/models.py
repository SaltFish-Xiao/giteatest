from django.db import models
from datetime import datetime,date,time
# Create your models here.

class Abcd(models.Model):
    id =  models.IntegerField(primary_key=True)
    date = models.DateField(null = True)
    personal = models.IntegerField(null=True)
    online = models.IntegerField(null=True)
    offline = models.IntegerField(null=True)
    # 添加其他需要的字段
    class Meta:
        # 指定模型对应的数据库表名
        db_table = 'user_abc'

class Details_yest(models.Model):
    id = models.IntegerField(primary_key=True)
    date = models.DateField(null=True) #昨日日期
    com_per = models.IntegerField(null=True) #昨日个单累计完成
    com_on = models.IntegerField(null=True) #昨日线上个单累计完成
    com_off = models.IntegerField(null=True) #昨日线下个单累计完成
    com_per_rate = models.DecimalField(max_digits=10,decimal_places=4,null=True)
    com_on_rate = models.DecimalField(max_digits=10,decimal_places=4,null=True)
    com_off_rate = models.DecimalField(max_digits=10,decimal_places=4,null=True)
    total_quantity = models.IntegerField(null=True)
    online_quantity = models.IntegerField(null=True)
    offline_quantity = models.IntegerField(null=True)
    group_quantity = models.IntegerField(null=True)
    total_tcr = models.DecimalField(max_digits=10,decimal_places=4,null=True)
    online_tcr = models.DecimalField(max_digits=10,decimal_places=4,null=True)
    offline_total_tcr = models.DecimalField(max_digits=10,decimal_places=4,null=True)