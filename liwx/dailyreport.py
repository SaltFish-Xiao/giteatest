#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'liwx.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)

if __name__ == '__main__':
    main()
    import pandas as pd
    from datetime import date, time, datetime, timedelta
    from user.models import *
    from django.db import connection


    yesterday = (datetime.now()-timedelta(days=1)).date()
    #os.chdir('D:/智慧医疗/')
    #导入目标表
    #data_target = pd.DataFrame(pd.read_excel('草稿.xlsx',sheet_name='Sheet2',index_col=0))
    #导入今日结果表
    #data_result = pd.DataFrame(pd.read_excel('每日统计_{}.xlsx'.
    #                                         format(datetime.strftime(
    #    (datetime.now()-timedelta(days=1)).date(),"%Y%m%d")),sheet_name='详情',index_col=0))
    #获取昨日日期
    datestr = datetime.strftime(yesterday,'%m/%d') #时间转换成特定格式字符串
    #获取前日日期
    for target in Abcd.objects.filter(date=yesterday):
        print(target.personal)
    #判断上升还是下降
    def diff(number):
        if number>0:
            return "提升"
        else:
            return "降低"
#查询累计单量的sql
    with connection.cursor() as cursor:
        cursor.execute("SELECT online FROM user_abc where id = 1")
        result = cursor.fetchall()
        for row in result:
            print(row[0])
    # print(
    # f"""
    # 截至到{datestr.split('/')[0]}月{datestr.split('/')[1]}日24点，
    # 个单目标{target_personnal}单，累计完成{data_result.iloc[0,0]}单，累计目标达成率{'{:.2%}'.format(data_result.iloc[0,0]/target_personnal)}，相较于前一天的目标达成率71.62%，提升1.35%。
    # 线上个单目标{online_target}单，累计完成{data_result.iloc[1,0]}单，累计目标达成率{'{:.2%}'.format(data_result.iloc[1,0]/online_target)}，相较于前一天的目标达成率66.07%，提升0.51%。
    # 线下个单目标{offline_target}单，累计完成236292单，累计目标达成率84.47%，相较于前一天的目标达成率81.26%，提升3.21%。
    # 31日推进情况如下
    # 1.当天总量13053，较前一日下降23384，目标完成率38.58%，较前一日提升0.64%；
    # 线上当日总量8435，较前一日下降4620，目标完成率24.64%，较前一日提升0.61%；
    # 线下当日总量个单4618，团单0，较前一日下降18764，目标完成率59.80%，较前一日提升0.63%。
    # 2.线下各保司日完成率如下：其中中国人保日销售量最高，太保产险日销量较前一日增幅最大。
    # 3.宣传推进群本日*保司提供宣传线索。截止当前*未提供新闻线索。
    # 4.团单方面，今日无变化。
    # """)
