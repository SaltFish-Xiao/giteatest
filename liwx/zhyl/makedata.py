from sqlalchemy import create_engine

engine = create_engine('mysql+pymysql://root:lwxLWX191315..@localhost:3306/LIWX')
from sqlalchemy import Column, String, Integer

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
class Target(Base):
    __tablename__ = 'user_target'
    id = Column(Integer, primary_key=True)
    total_target = Column(Integer)
    online_target = Column(Integer)
    offline_target = Column(Integer)

from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=engine)

session = Session()

targets = session.query(Target).filter(id=1)

for target in targets:

    print(target.total_target, target.online_target, target.offline_target)